#Import libraries
from bs4 import BeautifulSoup
from prettytable import PrettyTable
from termcolor import colored
import urllib.request , re , os , requests , inquirer

#Giving the function of the list of bags and their names on Almas Movie site
def ALmas_link_DL(URL , serial_name , Season_nuber):
    #Remove serial name spaces
    Serial_name = serial_name.replace(" " , "%20").title()

    #Fixed the problem of 0 behind numbers smaller than 10 in the link
    S = 0

    if int(Season_nuber) < 10 :
        S = "0" + str(Season_nuber)
    else:
        S = str(Season_nuber)

    #
    name_apper = []
    link_names = []
    href2=str()

    #serch Data name (fore name serial)
    serch_regex = re.compile(re.escape(serial_name), re.IGNORECASE)
    response0= requests.get(URL+"/?dir=Series/"+Serial_name[0])
    soup0 = BeautifulSoup(response0.text,"html.parser")

    for link in  soup0.find_all("a",{"data-name":serch_regex}):
        href2 = link.get("href")

    #Creating a download address
    url_DL = URL +"/"+ href2 + "/S"+ S

    #Grab web page elements
    response = requests.get(url_DL)
    Soup = BeautifulSoup(response.text,"html.parser")

    #Creating lists of quality addresses and their names
    List_of_qualities_names = []
    List_of_qualities_link = []

    #Finding the IDs of a on the page and hrefs and data names
    for Link in Soup.find_all("a"):
        Href = Link.get("href")
        Data_name = Link.get("data-name")

        #Find p in data name
        if Data_name and "p" in Data_name :

            #Liste link & name quality 
            List_of_qualities_link.append(Href)
            List_of_qualities_names.append(Data_name)

    #Check for quality
    if len(List_of_qualities_names) > 0 :

        #return lists
        return List_of_qualities_names , List_of_qualities_link
    else:
        List_of_qualities_names.append("ERR")
        List_of_qualities_link.append("ERR")
        return List_of_qualities_names , List_of_qualities_link

#Creat download link for quality selected by user
def ALmas_episode_DL(Serial_name,URL,DL_address):

    #Open file
    list_q = list()
    #Creating episode link
    episode_page_link = str(URL) + "/" + str(DL_address)

    #Grab web page elements
    response = requests.get(episode_page_link)    
    Soup = BeautifulSoup(response.text,"html.parser")

    #Find link and name episode
    for episode in Soup.find_all("a"):
        Href = episode.get("href")

        #Write in file link 
        Data_name = episode.get("data-name")
        if Data_name and "E" in Data_name :
            episode_links = URL + "/" + Href 
            list_q.append(episode_links)

    return list_q



#check the os
if os.name == 'nt':
    #clear konsol
    os.system('cls') # windwos
else:
    os.system('clear')  # Linux
#piint logo
logo = '''
                      _                  _ _
 _ __ ___   _____   _(_) ___          __| | |
| '_ ` _ \ / _ \ \ / / |/ _ \ _____  / _` | |
| | | | | | (_) \ V /| |  __/ _____ | (_| | |
|_| |_| |_|\___/ \_/ |_|\___|        \__,_|_|
'''

print(logo)
#check internet connectivity with try

z=0
def check_internet_connection():
    try:
        urllib.request.urlopen('https://duckduckgo.com')
        return True
    except:
        return False
if check_internet_connection():

    series_name=input( colored('Please enter the name of your TV series:', 'green'))
    season=input(colored('Please enter the number of the season: ', 'green'))


    #Url databace alamasmovie

    url_list=['https://tokyo.saymyname.website','https://nairobi.saymyname.website','https://rio.saymyname.website']
    good_url = ""
    c = 0
    #Find orginali adres 

    x = 0
    while z < len(url_list) and not good_url:
        ALmas_link_DL1 = ALmas_link_DL(url_list[c], series_name, season)
        c = c +1
        quality_link = ALmas_link_DL1[1]
        quality_name = ALmas_link_DL1[0]

        if quality_link[0] != "ERR":
            good_url = url_list[x]
        ALmas_link_DL1 = str()
        x=x+1

    z = 0
    tableـquality = PrettyTable()
    num_rows = len(quality_name)
    tableـquality.field_names = ["number", "quality"]
    for i in range(num_rows) :  
        q = quality_name[z]
        tableـquality.add_row([ z ,q ])
        z = z+1
    print(tableـquality)
    qulity =input("Please enter number of qulity :")
    episode = ALmas_episode_DL(series_name,good_url,quality_link[int(qulity)])
    txt_name = series_name +"_"+ "season"+"_"+season +".txt"
    file = open(txt_name, "w")
    file.write("\n".join(episode))
    print("end & link saved on ", txt_name )
else:
    print(colored("Please check your internet connection.","red"))
